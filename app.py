from flask import Flask, request, jsonify
from pymongo import MongoClient
from bson import ObjectId
from twilio.rest import Client
import random

app = Flask(__name__)
client = MongoClient("mongodb://127.0.0.1:27017/")
db = client["users_db"]
collection = db["users"]

# Twilio Credentials
account_sid = ""
auth_token = ""
twilio_phone_number = ""

twilio_client = Client(account_sid, auth_token)


def generate_otp():
    return str(random.randint(1000, 9999))


@app.route('/')
def home():
    return "<h1>API is running</h1>"


# Create a new user record
@app.route('/users', methods=["POST"])
def create_users():
    data = request.json
    otp = generate_otp()
    message = f"Your OTP for user registration is: {otp}"
    twilio_client.messages.create(to=data["phone"],
                                  from_=twilio_phone_number,
                                  body=message)
    data["otp"] = int(otp)
    result = collection.insert_one(data)
    return {"id": str(result.inserted_id)}, 201


# Verify the OTP and update the user record
@app.route('/users/verify_otp/<id>', methods=["POST"])
def verify_otp(id):
    data = request.json
    user = collection.find_one({"_id": ObjectId(id)})
    if user is None:
        return {"message": "User not found"}, 404
    if user.get("verified", False):
        return {"message": "User already verified"}, 400
    if user["otp"] != int(data["otp"]):
        return {"message": "Invalid OTP"}, 400

    result = collection.update_one({"_id": ObjectId(id)}, {
        "$set": {
            "verified": True
        },
        "$unset": {
            "otp": ""
        }
    })
    if result.modified_count == 0:
        return {"message": "User already verified"}, 404
    return {"message": "User verified"}, 200


# Get all user records
@app.route('/users', methods=["GET"])
def get_all_users():
    data = list(collection.find())
    for user in data:
        user["_id"] = str(user["_id"])
    return jsonify(data)


# Get a single user record by ID
@app.route("/users/<id>", methods=["GET"])
def get_user(id):
    data = collection.find_one({"_id": ObjectId(id)})
    if data is None:
        return {"message": "User not found"}, 404
    data["_id"] = str(data["_id"])
    return jsonify(data)


# Update a user record by ID
@app.route('/users/<id>', methods=["PUT"])
def update_user(id):
    data = request.json
    result = collection.update_one({"_id": ObjectId(id)}, {"$set": data})
    if result.modified_count == 0:
        return {"message": "User not found"}, 404
    return {"message": "User updated"}, 200


# Delete a user record by ID
@app.route('/users/<id>', methods=["DELETE"])
def delete_user(id):
    result = collection.delete_one({"_id": ObjectId(id)})
    if result.deleted_count == 0:
        return {"message": "User not found"}, 404
    return {"message": "User deleted"}, 200


if __name__ == '__main__':
    app.run(debug=True)
